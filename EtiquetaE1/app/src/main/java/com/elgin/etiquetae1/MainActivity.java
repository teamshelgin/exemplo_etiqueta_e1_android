package com.elgin.etiquetae1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.elgin.e1.Impressora.Etiqueta;

/** @author Gasperin **/
public class MainActivity extends AppCompatActivity {

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this.getApplicationContext();

        Button btnEt1 = findViewById(R.id.btnEt1);
        btnEt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                impEt1();
            }
        });

        Button btnEt2 = findViewById(R.id.btnEt2);
        btnEt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                impEt2();
            }
        });
    }

    private void impEt1() {
        Etiqueta.Limpa(0);
        Etiqueta.GerarBox(5, 100, 813, 267, 5, 5);
        Etiqueta.GerarTexto(1, 1, 2, 5, 155, 117, "ARROZ 5KG");
        Etiqueta.GerarBarCode1D( 1, 3, 6, 3, 100, 35, 290, "123456", 0);

        final String enderecoIP = "192.168.10.98"; // Troque pelo endereco IP da impressora...
        int retorno = Etiqueta.Imprime(3, "L42PRO", enderecoIP, 9100);
        mostrarRetorno(Integer.toString(retorno));
    }

    private void impEt2() {
        Etiqueta.Limpa(0);
        Etiqueta.GerarTexto(1, 2, 1, 1, 196, 10, "1234567890 PRODUTO TE");
        Etiqueta.GerarTexto(1, 2, 1, 1, 171, 10, "STE VENDE DE MAIS EST");
        Etiqueta.GerarTexto(1, 2, 1, 1, 146, 10, "E PRODUTO DE TESTE 01");
        Etiqueta.GerarTexto(1, 4, 1, 1, 95, 10, "R$ 9499,99");
        Etiqueta.GerarBarCode1D(1, 4, 1, 2, 65, 0, 10, "789456", 1);

        Etiqueta.GerarTexto(1, 2, 1, 1, 196, 360, "1234567890 PRODUTO TE");
        Etiqueta.GerarTexto(1, 2, 1, 1, 171, 360, "STE VENDE DE MAIS EST");
        Etiqueta.GerarTexto(1, 2, 1, 1, 146, 360, "E PRODUTO DE TESTE 01");
        Etiqueta.GerarTexto(1, 4, 1, 1, 95, 360, "R$ 9499,99");
        Etiqueta.GerarBarCode1D(1, 4, 1, 2, 65, 0, 360, "789456", 1);

        Etiqueta.GerarTexto(1, 2, 1, 1, 196, 710, "1234567890 PRODUTO TE");
        Etiqueta.GerarTexto(1, 2, 1, 1, 171, 710, "STE VENDE DE MAIS EST");
        Etiqueta.GerarTexto(1, 2, 1, 1, 146, 710, "E PRODUTO DE TESTE 01");
        Etiqueta.GerarTexto(1, 4, 1, 1, 95, 710, "R$ 9499,99");
        Etiqueta.GerarBarCode1D(1, 4, 1, 2, 65, 0, 710, "789456", 1);

        final String enderecoIP = "192.168.10.98"; // Troque pelo endereco IP da impressora...
        int retorno = Etiqueta.Imprime(3, "L42PRO", enderecoIP, 9100);
        mostrarRetorno(Integer.toString(retorno));
    }

    private void mostrarRetorno(String retorno) {
        Toast.makeText(mContext, String.format("Retorno: %s", retorno), Toast.LENGTH_SHORT).show();
    }
}
